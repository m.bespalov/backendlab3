<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="js/count.js" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

        <title>Задание №3</title>
</head>
<div class="contentblocks">
        <div class="container-sm theme-list py-3 pl-0 mb-3">
            <div class="d-flex flex-column align-items-center flex-wrap">
                <h2 class="text-center" id="form">Форма</h2>
<form class="d-block p-2" action="index.php" method="POST">
  <label>Your name</label>
  <input name="fio" type="text">
  <br>
  
  <label>Your email</label>
  <input name="email" type="email">
  <br>

  <label>Your year of birth</label>
  <select name="year_">
  <?php for($i = 1900; $i < 2020; $i++) {?>
  	<option value="<?php print $i; ?>"><?= $i; ?></option>
  	<?php }?>
  </select>
  <br>
  
  <p>Your sex</p>
  <label class="radio">
		<input type="radio" name="sex" value="0" checked>
		Male
  </label>
  <label class="radio">
		<input type="radio" name="sex" value="1">
		Female
  </label>
  <br>
  
  <p>Number of limbs</p>
  <label class="radio">
		<input type="radio" name="limbs" value="2" checked>
		4
  </label>
  <label class="radio">
		<input type="radio" name="limbs" value="40">
		40
  </label>
  <label class="radio">
		<input type="radio" name="limbs" value="41">
		41
  </label>
  <br>
  <br>
	
  <select name="abilities[]" multiple>
  	<option value="immort">Immortable</option>
  	<option value="wall">Passing through walls</option>
  	<option value="levit">Levitation</option>
  	<option value="invis">Invisible</option>
  	<option value="diffur">Diffur solution</option>
  </select>
  <br>
  
  <p>Your biography</p>
  <textarea name="text_" placeholder="Your biography" rows=3 cols=30></textarea>
  <br>
  
  <input type="checkbox" name="accept_" value="1">Accept
  <br>
  <input type="submit" value="Submit">
</form>
</div>
</div>
</div>
